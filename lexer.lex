%{
	#include <stdio.h>
%}

char 			[a-zA-Z]
digit 		[0-9]
space 		[ \t\n]
id 				#({char}|{digit})+
spc 			{space}
comment		(\/\/){char}+

%option yylineno

%%

{comment}						{/* Se ignora */}
{spc}* 					  	{/* Se ignora */}
\{|\}|\(|\)|\;|\,		{printf("<PUNCT_SYM,%s>\n", yytext);}
"=" 								{printf("<ASSIG_OP,%s>\n", yytext);}
(and)|(or)|(not)		{printf("<LOG_OP,%s>\n", yytext);}
>|<|(<=)|(>=)|(!=)	{printf("<REL_OP,%s>\n", yytext);}
\+|\-|\*|\/|\% 			{printf("<ARIT_OP,%s>\n", yytext);}
@if|@else|@while 		{printf("<KEYWORD,%s>\n", yytext);}
{id} 								{printf("<ID,%s>\n", yytext);}
{digit}+ 						{printf("<INT,%s>\n", yytext);}
{digit}*\.{digit}+	{printf("<DEC,%s>\n", yytext);}
.+ 									{printf("Error en la línea %d -> %s\n",yylineno,yytext);}

%%

int main (int argc, char **argv)
{
	FILE *f;
	
	f = fopen(argv[1], "r");
	yyin = f;
	yylex();
	fclose(yyin);
}
