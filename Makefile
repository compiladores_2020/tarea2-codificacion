OBJS = lex.yy.o
BIN = lexer

LIBS = -lfl
CC = gcc
FLEX = flex
CFLAGS = -c -g -O2 -Wall


$(BIN): $(OBJS)
	$(CC) -o $(BIN) $(OBJS) $(LIBS)

lex.yy.o: lex.yy.c
	$(CC) -c lex.yy.c 
	
lex.yy.c: lexer.lex
	$(FLEX) lexer.lex

clean:
	rm lexer *.c *.o
